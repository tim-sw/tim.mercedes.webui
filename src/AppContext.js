import React, { useState, createContext } from 'react';

export const AppContext = createContext();

window.configs = {};

export const AppContextProvider = (props) => {
    const [BBData, setBBData] = useState([]);
    const [selectedBBData, setSelectedBBData] = useState([]);
    const [faultyItems, setFaultyItems] = useState([]);
    const [isAuthenticated, setIsAuthenticated] = useState(false);
    const [loading, setLoading] = useState(false);
    const [toasts, setToasts] = useState([])

    return (
        <AppContext.Provider value={{BBData, setBBData, 
                                     selectedBBData, setSelectedBBData,
                                     faultyItems, setFaultyItems, 
                                     isAuthenticated, setIsAuthenticated,
                                     loading, setLoading,
                                     toasts, setToasts}}>
            {props.children}
        </AppContext.Provider>
    )
}
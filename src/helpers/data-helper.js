import axios from 'axios'
import { prepareRequestHeaders } from './common-helper';

const headers = {
    'Content-Type': 'application/json',
    'UserName': localStorage.getItem("userName")
}

export const getAllBBData = (successCallBack, errorCallBack) => {
    const apiUrl = window.app.config.API_BASE_URL + "/GetAllBBData"
    axios.post(apiUrl, null)
        .then(response => {
            if (!response.data.responseResult.result)
                errorCallBack(response.data.responseResult.exceptionDetail)
            else
                successCallBack(response.data.bbData ?? []);
        })
        .catch(error => errorCallBack(error));
}

export const getAllBBDatawithAllRelations = (successCallBack, errorCallBack) => {
    const apiUrl = window.app.config.API_BASE_URL + "/GetAllBBDataWithAllRelations"
    axios.post(apiUrl, null)
        .then(response => {
            if (!response.data.responseResult.result)
                errorCallBack(response.data.responseResult.exceptionDetail)
            else
                successCallBack(response.data.bbData ?? []);
        })
        .catch(error => errorCallBack(error));
}

export const getAllBBDataWithSeats = (successCallBack, errorCallBack) => {
    console.log("window.app.config.API_BASE_URL: " + window.app.config.API_BASE_URL);
    const apiUrl = window.app.config.API_BASE_URL + "/GetAllBBDataWithSeats"
    axios.post(apiUrl, null)
        .then(response => {
            
            
            if (!response.data.responseResult.result)
                errorCallBack(response.data.responseResult.exceptionDetail)
            else
                successCallBack(response.data.bbData ?? []);
        })
        .catch(error => errorCallBack(error));
}

export const getAllBBDataWithSeatsByCurrentWeek = (successCallBack, errorCallBack) => {
    console.log("window.app.config.API_BASE_URL: " + window.app.config.API_BASE_URL);
    const apiUrl = window.app.config.API_BASE_URL + "/GetAllBBDataWithSeatsByCurrentWeek"
    axios.post(apiUrl, null,{headers: prepareRequestHeaders()})
        .then(response => {
            if (!response.data.responseResult.result)
                errorCallBack(response.data.responseResult.exceptionDetail)
            else
                successCallBack(response.data.bbData ?? []);
        })
        .catch(error => errorCallBack(error));
}

export const getAllBBDataWithSeatsByDate = (startDate, endDate, successCallBack, errorCallBack) => {
    console.log("window.app.config.API_BASE_URL: " + window.app.config.API_BASE_URL);
    const apiUrl = window.app.config.API_BASE_URL + "/GetAllBBDataWithSeatsByDate"
    axios.post(apiUrl, {startDate, endDate}, {headers: prepareRequestHeaders()})
        .then(response => {
            if (!response.data.responseResult.result)
                errorCallBack(response.data.responseResult.exceptionDetail)
            else
                successCallBack(response.data.bbData ?? []);
        })
        .catch(error => errorCallBack(error));
}

export const getBBDataWithAllRelations = (bbNumber, successCallBack, errorCallBack) => {
    const request = {
        "BBNumber": bbNumber
    }
    const apiUrl = window.app.config.API_BASE_URL + "/GetBBDataWithAllRelations"
    axios.post(apiUrl, request)
        .then(response => {
            if (!response.data.responseResult.result)
                errorCallBack(response.data.responseResult.exceptionDetail)
            else
                successCallBack(response.data.bbData ?? []);
        })
        .catch(error => errorCallBack(error));
}

export const getAllFaultyBBs = (successCallBack, errorCallBack) => {
    const apiUrl = window.app.config.API_BASE_URL + "/GetAllFaultyBBs"
    axios.post(apiUrl, null)
        .then(response => {
            
            if (!response.data.responseResult.result)
                errorCallBack(response.data.responseResult.exceptionDetail)
            else
                successCallBack(response.data.faultyBBDetails ?? []);
        })
        .catch(error => errorCallBack(error));
}

export const getBBDataFromS2Gather = (startDate, endDate, successCallBack, errorCallBack) => {
    const apiUrl = window.app.config.API_BASE_URL + "/GetBBDataFromS2Gather"
    var data = {startDate, endDate};
    axios.post(apiUrl, data, {headers: prepareRequestHeaders()})
        .then(response => {
            
            if (!response.data.responseResult.result)
                errorCallBack(response.data.responseResult.exceptionDetail)
            else
                successCallBack(response.data.bbData ?? []);
        })
        .catch(error => errorCallBack(error));
}

export const getAllPairingHistory = (successCallBack, errorCallBack) => {
    const apiUrl = window.app.config.API_BASE_URL + "/GetAllPairingHistory"
    axios.post(apiUrl, null, {headers: prepareRequestHeaders()})
        .then(response => {
            debugger;
            if (!response.data.responseResult.result)
                errorCallBack(response.data.responseResult.exceptionDetail)
            else
                successCallBack(response.data.pairingHistory ?? []);
        })
        .catch(error => errorCallBack(error));
}
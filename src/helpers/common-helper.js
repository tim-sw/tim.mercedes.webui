import { STATUS_CODES, BBDATA_STATUS_CODES, FAULTY_BBDATA_STATUS_CODES, PAIRING_STATUS_CODES } from './constant';
import moment from 'moment';
import { toast } from 'react-toastify';

import {
    CBadge,
} from '@coreui/react'

export const toastCustom = {
    theme: "colored",
    success: function(message){
        toast.success(message, {theme: this.theme})
    },
    error: function(message){
        toast.error(message, {theme: this.theme})
    }
}

export const prepareRequestHeaders = () => {
    return {
        'Content-Type': 'application/json',
        'UserName': localStorage.getItem("userName")
    }
}

export const prepareSeatsTableColumns = () => {
    const columns = [
        //{name: "number", label: "Araç Numarası", options: {filter: true, sort: true}},
        //{name: "orderNumber", label: "Sipariş Numarası", options: {filter: true, sort: true}},
        {name: "type", label: "", options: {display: false, filter: false}},
        {name: "typeLabel", label: "Tip", options: {filter: false, sort: true}},
        //{name: "material", label: "Materyal", options: {filter: false, sort: true}},
        //{name: "description", label: "Açıklama", options: {filter: false, sort: true}},
        //{name: "date", label: "", options: {filter: false, display: false}},
        //{name: "dateStr", label: "Tarih", options: {filter: true, sort: true}},
        {name: "status", label: "", options: {display: false, filter: false}},
        {name: "statusLabel", label: "Durum", options: {filter: true, sort: true}},
    ];

    return columns
}

export const prepareBBDataTableData = (data) => {
    var arr = []
    data.forEach(item => {
        if (item.seats && item.seats != null) {
            var statusCode = Object.values(BBDATA_STATUS_CODES).find(p => p.value == item.status);
            var printedLength = item.seats.filter(p => p.status === STATUS_CODES.Printed.value).length
            arr.push({
                "bbNumber": item.bbNumber,
                "statusLabel": `${statusCode?.text} (${item.seats.length} / ${printedLength})`,
                "status": item.status
            })
        }
    });
    return arr;
}

export const prepareSeatsTableData = (data) => {
    data.forEach(element => {
        //var convertedDate = new Date(element.date);
        element["typeLabel"] = element.seatCode
        var statusCode = Object.values(STATUS_CODES).find(item => item.value == element.status);
        element["statusLabel"] = statusCode?.text;
        //resultArr.push([element.number , element.type, element.material, element.description, element.date, element.status]);
    });
    return data
}

export const prepareFaultyItemsTableColumns = () => {
    const columns = [
        {name: "bbNumber", label: "Araç Numarası", options: {filter: true, sort: true}},
        {name: "seatType", label: "Tip", options: {filter: true, sort: true}},
        {name: "seatCode", label: "Kod", options: {filter: true, sort: true}},
        {name: "date", label: "", options: {filter: false, display: false}},
        {name: "dateStr", label: "Tarih", options: {filter: true, sort: true}},
        {name: "imagePath", label: "Bağlantı", options: {filter: false, sort: false}},
        {name: "status", label: "", options: {display: false, filter: false}},
        {name: "statusLabel", label: "Durum", options: {filter: true, sort: true}},
        {name: "reProductionStatusLabel", label: "Yeniden Üretim", options: {filter: true, sort: true}},
        {name: "user", label: "Kullanıcı", options: {filter: true, sort: true}},
        {name: "printDate", label: "Baskı Tarihi", options: {filter: true, sort: true}},
    ];

    return columns
}

export const prepareFaultyItemsTableData = (data, onImgClick) => {
    data.forEach(element => {
        debugger;
        var convertedDate = new Date(element.faultyBB.date);
        element["dateStr"] = moment(convertedDate).format('DD.MM.YYYY')
        var statusCode = Object.values(FAULTY_BBDATA_STATUS_CODES).find(item => item.value == element.faultyBB.status);
        element["statusLabel"] = statusCode?.text;
        element["seatType"] = element.bbDetail.seatType;
        element["seatCode"] = element.seat.seatCode;
        element["bbNumber"] = element.faultyBB.bbNumber;
        element["imagePath"] = <img height="100" src={`${window.app.config.IMAGE_PATH}/${element.faultyBB.imgName}`} onClick={onImgClick}></img>;
        element["user"] = element.faultyBB.createdBy;
        var convertedPrintDate = new Date(element.seat.printDate);
        element["printDate"] = moment(convertedPrintDate).format('DD.MM.YYYY');
        element["reProductionStatusLabel"] = element.faultyBB.reProductionStatus ? "Evet" : "Hayır";
        //resultArr.push([element.number , element.type, element.material, element.description, element.date, element.status]);
    });
    return data
}

export const prepareMaterialsTableColumns = () => {
    const columns = [
        {name: "group", label: "Grup", options: {filter: true, sort: true}},
        {name: "materialNumber", label: "Materyal Numarası", options: {filter: true, sort: true}},
        {name: "name", label: "Ad", options: {filter: true, sort: true}},
        {name: "description", label: "Açıklama", options: {filter: true, sort: true}},
    ];

    return columns
}

export const prepareCushionsTableColumns = () => {
    const columns = [
        {name: "materialName", label: "Ad", options: {filter: true, sort: true}},
        {name: "materialNumber", label: "Materyal Numarası", options: {filter: true, sort: true}},
        {name: "unit", label: "Birim", options: {filter: true, sort: true}},
        {name: "quantity", label: "Adet", options: {filter: true, sort: true}},
    ];

    return columns
}

export const preparePairingHistoryReportTableColumns = () => {
    const columns = [
        { name: "userName", label: "Kullanıcı", options: { filter: true, sort: true } },
        { name: "createdOn", label: "Tarih", options: { filter: true, sort: true } },
        { name: "transportNumber", label: "Transport Bilgisi", options: { filter: true, sort: true } },
        { name: "bbNumber", label: "BB Bilgisi", options: { filter: true, sort: true } },
        {
            name: "status", label: "Durum", options: {
                filter: true,
                sort: true,
                customBodyRender: value => {
                    var statusCode = Object.values(PAIRING_STATUS_CODES).find(item => item.value == value);
                    var badgeColor = value == PAIRING_STATUS_CODES.CorrectPairing.value ? "success" : "warning";
                    return (
                        <h5>
                            <CBadge style={{fontSize: "14px !important"}} color={badgeColor}>
                                {statusCode?.text}
                            </CBadge>
                        </h5>
                    );
                }
            }
        }
    ]
    return columns
}

export const preparePairingHistoryReportTableData = (data) => {
    data.forEach(element => {
        debugger;
        var convertedDate = new Date(element.createdOn);
        element["createdOn"] = moment(convertedDate).format('DD.MM.YYYY')
        var statusCode = Object.values(PAIRING_STATUS_CODES).find(item => item.value == element.status);
        element["statusLabel"] = statusCode?.text;
        element["userName"] = element.createdBy;
    });
    return data
}


export const getLocalStorageBoolean = (key) =>  {
    return localStorage.getItem(key) === 'true';
}
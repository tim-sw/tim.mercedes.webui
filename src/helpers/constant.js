//export const API_BASE_URL = "http://localhost:5001/api/operation"
//export const API_BASE_URL = "http://localhost/service/operation"
//export const API_BASE_URL = "http://35.158.157.146:8080/service/operation"
//export const API_BASE_URL = "http://smart-tag.tr152.corpintra.net:8080/service/operation"

export const numberOfObjectsPerPage = 10;

export const TYPE_CODES = {
    Curtaion: {value: 1, text: "Perde"},
    Passenger_Seat: {value: 2, text: "Yolcu Koltuğu"},
    Stewardess_Seat: {value: 3, text: "Hostes Koltuğu"},
    Driver_Seat: {value: 4, text: "Sürücü Koltuğu"},
}

export const STATUS_CODES = {
    Printed: {value : 1, text: "Yazdırıldı"},
    Waiting: {value : 2, text: "Bekliyor"},
    Repaired: {value : 3, text: "Onarıldı"},
}

export const BBDATA_STATUS_CODES = {
    Waiting: {value : 1, text: "Bekliyor"},
    Completed : {value : 2, text: "Tamamlandı"},
}

export const FAULTY_BBDATA_STATUS_CODES = {
    Faulty: {value : 1, text: "Hatalı"},
    Damaged : {value : 2, text: "Hasarlı"},
    Repaired : {value : 3, text: "Onarıldı"},
    ReProduction : {value : 4, text: "Yeniden Üretim"},
}


export const PAIRING_STATUS_CODES = {
    CorrectPairing: {value : 1, text: "Doğru Eşleşme"},
    UserApprove: {value : 2, text: "Kullanıcı Onaylı"},
}

export const SEAT_PRINT_TYPES = {
    Cushion: {value : 1, text: "Minder"},
    BackRest: {value : 2, text: "Sırtlık"},
}
import axios from 'axios'
import { prepareRequestHeaders } from './common-helper';


export const login = (userName, password, successCallBack, errorCallBack) => {
    const apiUrl = window.app.config.API_BASE_URL + "/login"

    axios.post(apiUrl, { userName, password })
        .then(response => {
            if (!response.data.responseResult.result)
                errorCallBack(response.data.responseResult.exceptionDetail)
            else
                successCallBack(response.data);
        })
        .catch(error => errorCallBack(error));
}

export const printSelectedSeats = (selectedSeats, printType, successCallBack, errorCallBack) => {
    debugger;
    const apiUrl = window.app.config.API_BASE_URL + "/PrintSelectedSeats"
    axios.post(apiUrl, { "SeatDetails": selectedSeats, "PrintType": printType }, { headers: prepareRequestHeaders() })
        .then(response => {
            if (!response.data.responseResult.result)
                errorCallBack(response.data.responseResult.exceptionDetail)
            else
                successCallBack(response.data);
        })
        .catch(error => errorCallBack(error));
}

export const repairFaultyBBs = (selectedProducts, successCallBack, errorCallBack) => {

    const apiUrl = window.app.config.API_BASE_URL + "/RepairFaultyBBs"
    axios.post(apiUrl, { "FaultyBBs": selectedProducts }, { headers: prepareRequestHeaders() })
        .then(response => {
            if (!response.data.responseResult.result)
                errorCallBack(response.data.responseResult.exceptionDetail)
            else
                successCallBack(response.data);
        })
        .catch(error => errorCallBack(error));
}

export const setPairingHistory = (request, successCallBack, errorCallBack) => {
    const apiUrl = window.app.config.API_BASE_URL + "/SetPairingHistory";
    axios.post(apiUrl, request, { headers: prepareRequestHeaders() })
        .then(response => {
            if (!response.data.responseResult.result)
                errorCallBack(response.data.responseResult.exceptionDetail)
            else
                successCallBack(response.data);
        })
        .catch(error => errorCallBack(error));
}
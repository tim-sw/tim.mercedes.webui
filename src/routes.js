import React from 'react';

const Login = React.lazy(() => import('./pages/Login'));
const Main = React.lazy(() => import('./pages/Main'));
const ErrorManagement = React.lazy(() => import('./pages/ErrorManagement'));

const routes = [
  { path: '/', exact: true, name: 'Anasayfa' },
  { path: '/error_management', exact: true, name: 'Hata Yönetimi' },
];

export default routes;

import React from 'react';
import { HashRouter, Route, Switch } from 'react-router-dom';
import { AppContextProvider } from './AppContext';
import './scss/style.scss';
import './App.css';

const loading = (
  <div className="pt-3 text-center">
    <div className="sk-spinner sk-spinner-pulse"></div>
  </div>
)

const Login = React.lazy(() => import('./pages/Login'));
const Main = React.lazy(() => import('./pages/Main'));
const ErrorManagement = React.lazy(() => import('./pages/ErrorManagement'));
const Details = React.lazy(() => import('./pages/Details'));
const Pairing = React.lazy(() => import('./pages/Pairing'));
const PairingHistoryReport = React.lazy(() => import('./pages/reports/PairingHistoryReport'));

function App() {
  return (
    <HashRouter>
      <React.Suspense fallback={loading}>
        <AppContextProvider>
          <Switch>
            <Route exact path="/" name="Anasayfa" render={props => <Main {...props} />} />
            <Route exact path="/pairing" name="Eşleştirme" render={props => <Pairing {...props} />} />
            <Route exact path="/login" name="Giriş" render={props => <Login {...props} />} />
            <Route exact path="/error_management" name="Hata Yönetimi" render={props => <ErrorManagement {...props} />} />
            <Route exact path="/details" name="Details" render={props => <Details {...props} />} />
            <Route exact path="/pairing_history_report" name="Eşleştirme Raporu" render={props => <PairingHistoryReport {...props} />} />
          </Switch>
        </AppContextProvider>
      </React.Suspense>
    </HashRouter>
  );
}

export default App;

import React, { useContext, useState } from 'react'
import HomeIcon from '@material-ui/icons/Home';
import TableChart from '@material-ui/icons/TableChart';
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import AddBox from '@material-ui/icons/AddBox';
//import { useSelector, useDispatch } from 'react-redux'
import {
  CNavbar,
  CNavbarBrand,
  CCollapse,
  CNavbarNav,
  CNavLink,
  CToggler,
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import logo from '../content/logo-tim.jpg'

import { AppContext } from '../AppContext';

const Header = (props) => {
  const { } = useContext(AppContext);
  const [isOpen, setIsOpen] = useState(false)

  const onClickLogout = (e) => {
      localStorage.setItem("isAuthenticated", false);
      localStorage.setItem("userName", "");
      props.history.push("/")
  }

  return (
    <CNavbar expandable="sm" color="white" >
      <CToggler className="navbar__custom" inNavbar onClick={() => setIsOpen(!isOpen)} />
      <CNavbarBrand>
        <CIcon name="logo" height="80" alt="Logo" src={logo}/>
      </CNavbarBrand>
      <CCollapse show={isOpen} navbar>
        <CNavbarNav>
          <CNavLink className="header__blackText" exact to="/"><HomeIcon />Anasayfa</CNavLink>
          <CNavLink className="header__blackText" to="/error_management"><ErrorOutlineIcon />Hata Yönetim</CNavLink>
          <CNavLink className="header__blackText" to="/pairing"><AddBox />Transport ve Araç Eşleştirme</CNavLink>
          <CNavLink className="header__blackText" to="/pairing_history_report">
            <TableChart />Transport ve Araç Eşleştirme Raporu</CNavLink>
        </CNavbarNav>
        <CNavbarNav className="ml-auto">
          <CNavLink onClick={onClickLogout} className="header__blackText" to="/login">
          <ExitToAppIcon />Çıkış</CNavLink>
        </CNavbarNav>
      </CCollapse>
    </CNavbar>
  )
}

export default Header

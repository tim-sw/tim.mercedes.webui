import React from 'react'
import { CFooter } from '@coreui/react'

const Footer = () => {
  return (
    <CFooter fixed={false}>
      <div>
        <span className="ml-1">&copy; Copyright 2021 Thread In Motion All Rights Reserved</span>
      </div>
      <div className="mfs-auto">
        <span className="mr-1">TIM Seat Quality System</span>
      </div>
    </CFooter>
  )
}

export default React.memo(Footer)

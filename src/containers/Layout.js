import React, { useEffect, useContext } from 'react';
import { AppContext } from '../AppContext';
import Footer from '../containers/Footer';
import Header from '../containers/Header';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import BlockUi from "react-block-ui";
import "react-block-ui/style.css";

import { getLocalStorageBoolean } from '../helpers/common-helper'

const Layout = (props) => {
  const { loading } = useContext(AppContext);

  useEffect(() => {
    var isAuthenticated = getLocalStorageBoolean("isAuthenticated");
    if (!isAuthenticated)
      props.history.push("/login");
  }, [loading])

  return (
    <BlockUi
      blocking={loading}
      message="Yükleniyor"
      keepInView
    >
      <div className="c-app c-default-layout">
        <div className="c-wrapper">
          <Header {...props} />
          <div className="c-body">
            {props.children}
            <ToastContainer />
          </div>
          <Footer />
        </div>
      </div>
    </BlockUi>
  )
}

export default Layout;
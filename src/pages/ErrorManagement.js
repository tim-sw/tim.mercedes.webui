import React, { useState, useContext, useEffect } from 'react';
import RefreshIcon from '@material-ui/icons/Refresh';
import BuildIcon from '@material-ui/icons/Build';
import {
    CButton,
    CCard,
    CCardBody,
    CCol,
    CRow,
    CModal,
    CModalBody,
} from '@coreui/react'
import Layout from '../containers/Layout';

import { getAllFaultyBBs } from '../helpers/data-helper';
import { repairFaultyBBs } from '../helpers/business-helper';
import { prepareFaultyItemsTableData, prepareFaultyItemsTableColumns, toastCustom } from '../helpers/common-helper';
import { FAULTY_BBDATA_STATUS_CODES } from '../helpers/constant';

import MUIDataTable from "mui-datatables";
import { AppContext } from '../AppContext';

const ErrorManagement = (props) => {
    const { faultyItems, setFaultyItems, setLoading } = useContext(AppContext);
    const [selectedItems, setSelectedItems] = useState([]);
    const [modal, setModal] = useState(false);
    const [modalImageSrc, setModalImageSrc] = useState();

    const onImgClick = (event) => {
        
        setModalImageSrc(event.currentTarget.src)
        setModal(true);
    }

    useEffect(() => {
        setLoading(true)
        getAllFaultyBBs(response => {
            var data = prepareFaultyItemsTableData(response, onImgClick)
            setFaultyItems(data)
            setLoading(false)
        },
            error => {
                
                toastCustom.error(error.toString());
                setLoading(false)
            });
    }, [props.location])

    const onClickRefresh = () => {
        //todo : axios post will be added
        setLoading(true)
        getAllFaultyBBs(response => {
            
            var data = prepareFaultyItemsTableData(response, onImgClick)
            setFaultyItems(data)
            setLoading(false)
        },
            error => {
                
                toastCustom.error(error.toString());
                setLoading(false)
            });
    }

    const onClickRepair = () => {
        //todo : axios post will be added
        setLoading(true)
        repairFaultyBBs(selectedItems, response => {
            toastCustom.success("İşlem başarılı");
            setLoading(false)
            onClickRefresh();
        },
            error => {
                
                toastCustom.error(error.toString());
                setLoading(false)
            });
    }

    const onRowSelectionChange = (ev, ex, ez) => {
        
        var internalData = [...selectedItems]
        if (ev.length === 0) {
            //clear array
            internalData.splice(0, internalData.length);
        }
        else {
            var data = faultyItems[ev[0]["index"]];
            var dataIndex = internalData.findIndex(item => item.id == data.id)
            if (dataIndex > -1) {
                internalData.splice(dataIndex, 1);
            }
            else {
                let { imageBase64, ...finalData } = data.faultyBB;
                internalData.push(finalData);
            }
        }
        setSelectedItems(internalData);
    }

    const isRowSelectable = (index) => {
        var data = faultyItems[index];
        if (data.faultyBB.status != FAULTY_BBDATA_STATUS_CODES.Repaired.value)
            return true;
        return false;
    }

    const options = {
        filterType: 'checkbox',
        print: false,
        downloadOptions: {
            filename: "data.xlsx",
            filterOptions: {
                useDisplayedColumnsOnly: true,
                useDisplayedRowsOnly: true
            }
        },
        onRowSelectionChange,
        isRowSelectable
    };

    return (
        <Layout {...props}>
            <CCard className="transparentCard">
                <CCardBody>
                    <CRow style={{ paddingBottom: "10px" }}>
                        <CCol md="2" >
                            <CButton onClick={onClickRefresh} size="100" block color="success">Listeyi Güncelle <RefreshIcon /></CButton>
                        </CCol>
                        <CCol md="8" ></CCol>
                        <CCol md="2" >
                            <CButton onClick={onClickRepair} block color="info">Seçilenleri Onar <BuildIcon /></CButton>
                        </CCol>
                    </CRow>
                    <CRow>
                        <CCol md="12">
                            <MUIDataTable
                                title={"Hata Listesi"}
                                data={faultyItems}
                                columns={prepareFaultyItemsTableColumns()}
                                options={options}
                            />
                        </CCol>
                    </CRow>
                </CCardBody>
            </CCard>

            <CModal show={modal} onClose={setModal}>
                <CModalBody style={{textAlign:"center"}}>
                    <img src={modalImageSrc} width="100%"></img>
                </CModalBody>
                {/* <CModalFooter>
                    <CButton color="secondary" onClick={() => setModal(false)}>Cancel</CButton>
                </CModalFooter> */}
            </CModal>
        </Layout>
    );
}

export default ErrorManagement;
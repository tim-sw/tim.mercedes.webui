import React, { useState, useContext } from 'react';
import { AppContext } from '../AppContext';
import {
    CButton,
    CCard,
    CCardBody,
    CCardGroup,
    CCol,
    CContainer,
    CForm,
    CInput,
    CInputGroup,
    CInputGroupPrepend,
    CInputGroupText,
    CRow
} from '@coreui/react'
import { login } from '../helpers/business-helper'

import logo from '../content/logo-white-tim.png'
import PersonOutlineIcon from '@material-ui/icons/PersonOutline';
import LockOpenIcon from '@material-ui/icons/LockOpen';
import 'react-toastify/dist/ReactToastify.css';
import { toastCustom } from '../helpers/common-helper'
import { ToastContainer } from 'react-toastify';

import BlockUi from "react-block-ui";
import "react-block-ui/style.css";

const Login = (props) => {
    const [userName, setUserName] = useState("");
    const [password, setPassword] = useState("");
    const { loading, setLoading } = useContext(AppContext);

    const onClickLogin = (e) => {
        setLoading(true);
        login(userName, password,
            (response) => {
                
                localStorage.setItem("isAuthenticated", true);
                localStorage.setItem("userName", userName);
                setLoading(false);
                
                props.history.push("/");
            },
            (err) => {
                
                toastCustom.error(err.toString());
                setLoading(false);
                localStorage.setItem("isAuthenticated", false);
                localStorage.setItem("userName", null);
                console.log(err);
            })
    }

    return (
        <BlockUi
            blocking={loading}
            message="Yükleniyor"
            keepInView
        >
            <div className="c-app c-default-layout flex-row align-items-center">
                <CContainer>
                    <CRow className="justify-content-center">
                        <CCol md="8">
                            <CCardGroup>
                                <CCard className="p-4">
                                    <CCardBody>
                                        <CForm>
                                            <h1>Giriş</h1>
                                            <CInputGroup className="mb-3">
                                                <CInputGroupPrepend>
                                                    <CInputGroupText>
                                                        <PersonOutlineIcon style={{ fontSize: "18px" }} />
                                                    </CInputGroupText>
                                                </CInputGroupPrepend>
                                                <CInput type="text" placeholder="Kullanıcı Adı" autoComplete="username" value={userName} onChange={(e) => setUserName(e.target.value)} />
                                            </CInputGroup>
                                            <CInputGroup className="mb-4">
                                                <CInputGroupPrepend>
                                                    <CInputGroupText>
                                                        <LockOpenIcon style={{ fontSize: "18px" }} />
                                                    </CInputGroupText>
                                                </CInputGroupPrepend>
                                                <CInput type="password" placeholder="Şifre" autoComplete="current-password" value={password} onChange={(e) => setPassword(e.target.value)} />
                                            </CInputGroup>
                                            <CRow>
                                                <CCol xs="6">
                                                    <CButton onClick={onClickLogin} color="primary" className="px-4">Giriş</CButton>
                                                </CCol>
                                            </CRow>
                                        </CForm>
                                    </CCardBody>
                                </CCard>
                                <CCard className="text-white bg-primary py-5 d-md-down-none" style={{ width: '44%' }}>
                                    <CCardBody className="text-center">
                                        <div>
                                            <img src={logo} style={{ marginTop: "40px" }} />
                                        </div>
                                    </CCardBody>
                                </CCard>
                            </CCardGroup>
                        </CCol>
                    </CRow>
                    
            <ToastContainer />
                </CContainer>
            </div>
        </BlockUi>
    )
}

export default Login
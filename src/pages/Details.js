import React, { useState, useContext, useEffect } from 'react';
import { AppContext } from '../AppContext';
import Layout from '../containers/Layout';

import {
    CCard,
    CCardBody,
    CCol,
    CRow,
    CCardHeader
} from '@coreui/react'

import { getBBDataWithAllRelations } from '../helpers/data-helper'
import { prepareMaterialsTableColumns, prepareCushionsTableColumns, toastCustom } from '../helpers/common-helper'

import MUIDataTable from "mui-datatables";

const Details = (props) => {

    const { setLoading, selectedBBData } = useContext(AppContext);

    const [selectedBBDataData, setSelectedBBDataData] = useState({});
    const [materialTableData, setMaterialTableData] = useState([]);
    const [cushionTableData, setCushionTableData] = useState([]);

    useEffect(() => {
        setLoading(true);
        // todo - ozan: retrieve details by given BBData

        getBBDataWithAllRelations(selectedBBData, 
            (response) => {
                
                setSelectedBBDataData(response)
                setMaterialTableData(response.materials)
                setCushionTableData(response.cushions)
                setLoading(false);
            },
            (error) => {
                setLoading(false);
                toastCustom.error(error.toString());
                console.log(error);
            })
    }, []);

    const options = {
        filterType: 'checkbox',
        selectableRows:"none",
        downloadOptions: {
            filename: "data.xlsx",
            filterOptions: {
                useDisplayedColumnsOnly: true,
                useDisplayedRowsOnly: true
            }
        }
    };

    return (
        <Layout {...props} >
            <CCard className="transparentCard">
                <CCardBody>
                    <CRow>
                        <CCol xs="12" sm="6" md="12">
                            <CCard accentColor="warning">
                                <CCardHeader>
                                    <h2>BB Detayı</h2>
                                </CCardHeader>
                                <CCardBody>
                                    {
                                        selectedBBDataData && selectedBBDataData.bbDetails && selectedBBDataData.bbDetails.length > 0 && 
                                        <blockquote className="card-bodyquote">
                                            <p><b>BB No: </b> {selectedBBDataData.bbNumber}</p>
                                            <p><b>Açıklama: </b> {selectedBBDataData?.bbDetails[0]?.explanation1}</p>
                                            <p>{selectedBBDataData?.bbDetails[0]?.explanation2}</p>
                                            <p><b>Koltuk Tipi: </b> {selectedBBDataData?.bbDetails[0]?.seatType}</p>
                                        </blockquote>
                                    }
                                </CCardBody>
                            </CCard>
                        </CCol>
                        <CCol md="6">
                            <MUIDataTable title={"Kumaş Listesi"} data={materialTableData} columns={prepareMaterialsTableColumns()} options={options} />
                        </CCol>
                        <CCol md="6">
                            <MUIDataTable title={"Minder Listesi"} data={cushionTableData} columns={prepareCushionsTableColumns()} options={options} />
                        </CCol>
                    </CRow>
                </CCardBody>
            </CCard>
        </Layout>
    );
}

export default Details;
import { CCard, CCardBody, CCol, CRow, CLabel, CInput, CButton, CModalHeader, CModalTitle, CModalBody, CModalFooter, CModal } from '@coreui/react';
import React, { useContext, useState } from 'react';
import { AppContext } from '../AppContext';
import Layout from '../containers/Layout';
import { setPairingHistory } from '../helpers/business-helper';
import { toastCustom } from '../helpers/common-helper';
import { PAIRING_STATUS_CODES } from '../helpers/constant';

const Pairing = (props) => {
    const { setLoading, setToasts } = useContext(AppContext);

    const [confirmationModalVisibility, setConfirmationModalVisibility] = useState(false);

    const [bbNumber, setBbNumber] = useState();
    const [transportNumber, setTransportNumber] = useState();

    const onApproveProcess = () => {
        setConfirmationModalVisibility(false);
        setLoading(true)
        var request = {
            bbNumber,
            transportNumber,
            status: PAIRING_STATUS_CODES.UserApprove.value
        };
        setPairingHistory(request, serviceSuccessCallBack, serviceErrorCallBack)
    }

    const onClickMatchButton = () => {
        if (!bbNumber || !transportNumber) {
            toastCustom.error("Değer boş bırakılamaz");
            return;
        }
        if (bbNumber !== transportNumber) {
            setConfirmationModalVisibility(true);
            return;
        }
        setLoading(true)

        var request = {
            bbNumber,
            transportNumber,
            status: PAIRING_STATUS_CODES.CorrectPairing.value
        };
        setPairingHistory(request, serviceSuccessCallBack, serviceErrorCallBack)
    }

    const serviceSuccessCallBack = () => {
        setLoading(false);
        setBbNumber("");
        setTransportNumber("");
        toastCustom.success("İşlem Başarılı");
    }

    const serviceErrorCallBack = (error) => {
        setLoading(false);
        toastCustom.error(error.toString());
    }

    return (
        <Layout {...props}>
            <CCol md={12}>
                <CCard style={{ top: "20px" }}>
                    <CCardBody>
                        <CRow style={{ paddingBottom: "10px" }} className="align-items-end">
                            <CCol md="4">
                                <CLabel htmlFor="bbNumber">BB Numarası</CLabel>
                                <CInput type="text" id="bbNumber" name="bbNumber" value={bbNumber} onChange={(e) => setBbNumber(e.currentTarget.value)} placeholder="BB Numarası" />
                            </CCol>
                            <CCol md="4">
                                <CLabel htmlFor="transportNumber">Transport Numarası</CLabel>
                                <CInput type="text" id="transportNumber" name="transportNumber" value={transportNumber} onChange={(e) => setTransportNumber(e.currentTarget.value)} placeholder="Transport Numarası" />
                            </CCol>
                            <CCol md="4" >
                                <CButton onClick={onClickMatchButton} block color="success">Eşleştir </CButton>
                            </CCol>
                        </CRow>
                    </CCardBody>
                </CCard>
            </CCol>

            <CModal
                show={confirmationModalVisibility}
                onClose={() => setConfirmationModalVisibility(!confirmationModalVisibility)}
                color="warning"
            >
                <CModalHeader closeButton>
                    <CModalTitle>Uyarı</CModalTitle>
                </CModalHeader>
                <CModalBody>
                    <p>Girilen değerler eşit değil!</p> 
                    <p>Yapılan işlemi onaylıyor musunuz ?</p>
                </CModalBody>
                <CModalFooter>
                    <CButton color="success" onClick={() => onApproveProcess()}>Onalya</CButton>{' '}
                    <CButton color="secondary" onClick={() => setConfirmationModalVisibility(!confirmationModalVisibility)}>İptal</CButton>
                </CModalFooter>
            </CModal>
        </Layout>
    );
}

export default Pairing;
import React, { useState, useEffect, useContext } from 'react';
import { AppContext } from '../../AppContext';
import { PAIRING_STATUS_CODES } from '../../helpers/constant';
import Layout from '../../containers/Layout';
import { toastCustom, preparePairingHistoryReportTableColumns, preparePairingHistoryReportTableData } from '../../helpers/common-helper';
import { getAllPairingHistory } from '../../helpers/data-helper';

import RefreshIcon from '@material-ui/icons/Refresh';

import {
    CCard,
    CCardBody,
    CCol,
    CRow,
    CButton,
} from '@coreui/react'

import MUIDataTable from "mui-datatables";

const PairingHistoryReport = (props) => {
    const { setLoading } = useContext(AppContext);

    const [pairingHistoryData, setPairingHistoryData] = useState();

    useEffect(() => {
        setLoading(true);

        getAllPairingHistory((response) => {
            setPairingHistoryData(preparePairingHistoryReportTableData(response))
            setLoading(false);
        },
            (error) => {
                toastCustom.error(error.toString());
                setLoading(false);
            })
    }, [])

    const onClickRefresh = () => {
        
        //todo : axios post will be added
        setLoading(true)
        getAllPairingHistory(response => {
            setPairingHistoryData(preparePairingHistoryReportTableData(response));
            setLoading(false)
        },
            error => {
                
                toastCustom.error(error.toString());
                setLoading(false)
            });
    }

    const options = {
        responsive: 'standard',
        selectableRows: "none",
        print: false,
        filterType: "dropdown",

    }

    return (
        <Layout {...props}>
            <CCard className="transparentCard">
                <CCardBody>
                    <CRow style={{ paddingBottom: "10px" }}>
                        <CCol md="2" >
                            <CButton onClick={onClickRefresh} block color="success">Listeyi Güncelle <RefreshIcon /></CButton>
                        </CCol>
                    </CRow>
                    <CRow>
                        <CCol xs="12" lg="12">
                            <MUIDataTable title={"Transport ve Araç Eşleştirme Raporu"} data={pairingHistoryData} columns={preparePairingHistoryReportTableColumns()} options={options} />
                        </CCol>
                    </CRow>
                </CCardBody>
            </CCard>
        </Layout>);
}

export default PairingHistoryReport;
import React, { useState, useEffect, useContext } from 'react';
import RefreshIcon from '@material-ui/icons/Refresh';
import LocalPrintshopIcon from '@material-ui/icons/LocalPrintshop';
import FindInPageIcon from '@material-ui/icons/FindInPage';
import VisibilityIcon from '@material-ui/icons/Visibility';
import CancelOutlined from '@material-ui/icons/CancelOutlined';

import {
    CButton,
    CCard,
    CCardBody,
    CCol,
    CRow,
    CInput,
    CLabel,
    CModal,
    CModalHeader,
    CModalTitle,
    CModalBody,
    CModalFooter,
    CListGroup,
    CListGroupItem,
    CBadge,
    CFormGroup,
    CInputRadio,
} from '@coreui/react'
import Layout from '../containers/Layout';
import { printSelectedSeats } from '../helpers/business-helper';
import { getAllBBDataWithSeatsByCurrentWeek, getAllBBDataWithSeatsByDate } from '../helpers/data-helper';
import { prepareBBDataTableData, prepareSeatsTableData, prepareSeatsTableColumns, getLocalStorageBoolean, toastCustom } from '../helpers/common-helper';

import MUIDataTable from "mui-datatables";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import { AppContext } from '../AppContext';
import { SEAT_PRINT_TYPES } from '../helpers/constant';

const MainPage = (props) => {

    const { BBData, setBBData, setLoading, setSelectedBBData } = useContext(AppContext);

    const [selectedSeats, setSelectedSeats] = useState([]);

    const [mainTableData, setMainTableData] = useState([]);

    const [startDate, setStartDate] = useState("");

    const [endDate, setEndDate] = useState("");

    const [selectedHeaderIndexes, setSelectedHeaderIndexes] = useState([]);

    const [confirmationModalVisibility, setConfirmationModalVisibility] = useState(false);

    const [selectedPrintType, setSelctedPrintType] = useState(SEAT_PRINT_TYPES.BackRest.value);

    useEffect(() => {
        var isAuthenticated = getLocalStorageBoolean("isAuthenticated");
        if (isAuthenticated) {
            debugger;
            setLoading(true)
            if (BBData && BBData.length > 0) {
                var data = prepareBBDataTableData(BBData)
                setMainTableData(data);
                setLoading(false)
            }
            else {
                getAllBBDataWithSeatsByCurrentWeek(response => {
                    var data = prepareBBDataTableData(response)
                    setMainTableData(data);
                    setBBData(response);
                    setLoading(false)
                },
                    error => {
                        toastCustom.error(error.toString());
                        setLoading(false)
                    });
            }
        }
    }, [props.location])


    const onClickRefresh = () => {

        //todo : axios post will be added
        setLoading(true)
        getAllBBDataWithSeatsByCurrentWeek(response => {
            var data = prepareBBDataTableData(response);
            setMainTableData(data);
            setBBData(response)
            setLoading(false)
        },
            error => {

                toastCustom.error(error.toString());
                setLoading(false)
            });
    }

    const printProcess = () => {
        //todo : axios post will be added
        setConfirmationModalVisibility(false);
        setLoading(true)
        printSelectedSeats(selectedSeats, selectedPrintType,
            response => {

                setSelectedSeats([]);
                setSelectedHeaderIndexes([])
                toastCustom.success("İşlem başarılı");
                setLoading(false)
                //onClickRefresh();
            },
            error => {

                toastCustom.error(error);
                setLoading(false)
            });
    }

    const onClickPrint = () => {
        //todo : axios post will be added

        if (selectedHeaderIndexes.length === 0) {
            toastCustom.error("Lütfen seçim yapınız!");
            return;
        }
        setConfirmationModalVisibility(true);
    }

    const onDetailsClick = (e) => {

        setSelectedBBData(e);
        //setIsAuthenticated(true);
        props.history.push({
            pathname: "/details",
            state: { selectedBBData: e }
        })
    }

    const onSearchClick = () => {
        if (!startDate || !endDate) {
            toastCustom.error("Lütfen tarih seçini.");
            return;
        }

        setLoading(true)
        getAllBBDataWithSeatsByDate(startDate, endDate, response => {
            var data = prepareBBDataTableData(response);
            setMainTableData(data);
            setBBData(response)
            setLoading(false)
        },
            error => {

                toastCustom.error(error.toString());
                setLoading(false)
            });
    }

    const onRowSelectionChange = (expandedBBData, dataIndexArr) => {

        var bbDataIndex = BBData.findIndex(p => p.bbNumber === expandedBBData.bbNumber);

        // if there is no selected data on the expanded table remove all bb data from list
        if (dataIndexArr.length === 0) {
            //clear array
            setSelectedSeats(selectedSeats.filter(p => p.bbNumber !== expandedBBData.bbNumber));
            setSelectedHeaderIndexes(selectedHeaderIndexes.filter(p => p !== bbDataIndex));
        }
        else {
            // if all expanded bb seats is selected set header index as selected
            if (!selectedHeaderIndexes.includes(bbDataIndex)) {
                setSelectedHeaderIndexes([...selectedHeaderIndexes, bbDataIndex]);
            }

            // loop all selected seat data and add to list
            dataIndexArr.forEach(item => {
                var selectedData = {
                    seat: expandedBBData.seats[item["index"]],
                    bbNumber: expandedBBData.bbNumber,
                    materials: expandedBBData.materials,
                    //bbDetail: expandedBBData.bbDetails && expandedBBData.bbDetails.length > 0 && expandedBBData.bbDetails[0]
                }
                var dataIndex = selectedSeats.findIndex(item => item.seat.id == selectedData.seat.id)
                if (dataIndex > -1) {
                    selectedSeats.splice(dataIndex, 1);
                }
                else {
                    selectedData.isAllSeatsSelected = expandedBBData.seats.length == (selectedSeats.filter(p => p.bbNumber === expandedBBData.bbNumber).length + 1) ? true : false; // +1 is for the selected seats push,;
                    selectedSeats.push(selectedData);
                }
            });
        }
    }

    const onHeaderRowSelectionChange = (ev, ex, ez) => {
        debugger;
        var mainBbNumberData = mainTableData[ev[0].dataIndex];

        var bbNumberData = BBData.find(p => p.bbNumber == mainBbNumberData.bbNumber)
        // remove all seats from list : unchecked main input
        if (selectedSeats.some(p => p.bbNumber === bbNumberData.bbNumber)) {
            var filteredData = selectedSeats.filter(p => p.bbNumber !== bbNumberData.bbNumber)
            setSelectedSeats(filteredData);
            setSelectedHeaderIndexes(selectedHeaderIndexes.filter(p => p !== ev[0].dataIndex));

        }
        // add all seats data to list : checked main input
        else {
            if (bbNumberData && bbNumberData.seats && bbNumberData.seats.length > 0) {
                bbNumberData.seats.forEach((item) => {
                    selectedSeats.push({
                        seat: item,
                        bbNumber: bbNumberData.bbNumber,
                        materials: bbNumberData.materials,
                        //bbDetail: bbNumberData.bbDetails && bbNumberData.bbDetails.length > 0 && bbNumberData.bbDetails[0]
                    })
                })
                selectedHeaderIndexes.push(ev[0].dataIndex);
                setSelectedHeaderIndexes([...selectedHeaderIndexes])
            }
        }

    }

    const onRemoveSelectedHeaderClick = (headerIndex, bbNumberData) => {
        debugger;
        // remove all seats from list : unchecked main input
        var filteredData = selectedSeats.filter(p => p.bbNumber !== bbNumberData.bbNumber)
        setSelectedSeats(filteredData);
        setSelectedHeaderIndexes(selectedHeaderIndexes.filter(p => p !== headerIndex));
    }

    const columns = [
        {
            name: "bbNumber",
            label: "BB No",
            options: {
                filter: true,
            }
        },
        {
            name: "detailsAction",
            label: "Detay",
            options: {
                customBodyRender: (value, tableMeta, updateValue) => {
                    return (
                        <CRow>
                            <CCol md="3" >
                                <CButton onClick={() => onDetailsClick(tableMeta.rowData[0])} size="lg" block color="warning"><VisibilityIcon /></CButton>
                            </CCol>
                        </CRow>
                    )
                }
            }
        },
        { name: "status", label: "", options: { display: false, filter: false } },
        { name: "statusLabel", label: "Durum", options: { filter: true } },
    ]

    const options = {
        responsive: 'standard',
        print: false,
        expandableRows: true,
        expandableRowsHeader: false,
        expandableRowsOnClick: true,
        selectableRowsHeader: false,
        selectToolbarPlacement: "none",
        rowsSelected: selectedHeaderIndexes,
        renderExpandableRow: (rowData, rowMeta) => {
            const colSpan = rowData.length + 1;
            var data = BBData.find(item => item.bbNumber === rowData[0])
            var selectedDataIndexes = [];
            if (data && data != null) {
                selectedSeats.forEach(item => {
                    var itemIndex = data.seats.findIndex(p => p.id === item.seat.id)
                    if (itemIndex >= 0)
                        selectedDataIndexes.push(itemIndex);
                })
            }
            const options = {
                filterType: 'checkbox',
                rowsSelected: selectedDataIndexes,
                print: false,
                downloadOptions: {
                    filename: "data.xlsx",
                    filterOptions: {
                        useDisplayedColumnsOnly: true,
                        useDisplayedRowsOnly: true
                    }
                },
                onRowSelectionChange: (ev, ex, ez) => onRowSelectionChange(data, ev)
            };
            return (
                <TableRow>
                    <TableCell colSpan={colSpan}>
                        <CCard className="transparentCard">
                            <CCardBody>
                                <CRow>
                                    <CCol md="12">
                                        <MUIDataTable
                                            title={"Koltuk Listesi"}
                                            data={prepareSeatsTableData(data.seats)}
                                            columns={prepareSeatsTableColumns()}
                                            options={options}
                                        />
                                    </CCol>
                                </CRow>
                            </CCardBody>
                        </CCard>
                    </TableCell>
                </TableRow>
            );
        },
        onRowSelectionChange: (ev, ex, ez) => onHeaderRowSelectionChange(ev, ex, ez)
    }

    return (
        <Layout {...props}>
            <CCard className="transparentCard">
                <CCardBody>
                    <CRow style={{ paddingBottom: "10px" }}>
                        <CCol md="2" >
                            <CButton onClick={onClickRefresh} block color="success">Listeyi Güncelle <RefreshIcon /></CButton>
                        </CCol>
                        <CCol md="8" ></CCol>
                        <CCol md="2" >
                            <CButton onClick={onClickPrint} block color="info">Seçilenleri Yazdır <LocalPrintshopIcon /></CButton>
                        </CCol>
                    </CRow>
                    <CRow style={{ paddingBottom: "10px" }} className="align-items-end">
                        <CCol md="4">
                            <CLabel htmlFor="date-input">Başlangıç Tarihi</CLabel>
                            <CInput type="date" id="date-input" name="date-input" value={startDate} onChange={(e) => setStartDate(e.currentTarget.value)} placeholder="date" />
                        </CCol>
                        <CCol md="4">
                            <CLabel htmlFor="date-input">Bitiş Tarihi</CLabel>
                            <CInput type="date" id="date-input" name="date-input" value={endDate} onChange={(e) => setEndDate(e.currentTarget.value)} placeholder="date" />
                        </CCol>
                        <CCol md="4" >
                            <CButton onClick={onSearchClick} block color="success">Ara <FindInPageIcon /></CButton>
                        </CCol>
                    </CRow>
                    <CRow>
                        <CCol md="12">
                            <MUIDataTable title={"Araç Listesi"} data={mainTableData} columns={columns} options={options} />
                        </CCol>
                    </CRow>
                </CCardBody>
            </CCard>

            <CModal
                show={confirmationModalVisibility}
                onClose={() => setConfirmationModalVisibility(!confirmationModalVisibility)}
                color="warning"
            >
                <CModalHeader closeButton>
                    <CModalTitle>BB Detayı</CModalTitle>
                </CModalHeader>
                <CModalBody>
                    <CCol style={{ paddingBottom: "10px", textAlign: "center" }}>
                        <CFormGroup variant="custom-radio" inline>
                            <CInputRadio custom id="backrest" name="inline-radios"  onChange={() => setSelctedPrintType(SEAT_PRINT_TYPES.BackRest.value)} value={SEAT_PRINT_TYPES.BackRest.value} />
                            <CLabel variant="custom-checkbox" htmlFor="backrest">{SEAT_PRINT_TYPES.BackRest.text} </CLabel>
                        </CFormGroup>
                        <CFormGroup variant="custom-radio" inline>
                            <CInputRadio custom id="cushion" name="inline-radios" onChange={() => setSelctedPrintType(SEAT_PRINT_TYPES.Cushion.value)} value={SEAT_PRINT_TYPES.Cushion.value} />
                            <CLabel variant="custom-checkbox" htmlFor="cushion">{SEAT_PRINT_TYPES.Cushion.text}</CLabel>
                        </CFormGroup>
                    </CCol>
                    <CCol>
                        {
                            selectedHeaderIndexes.map(item => {
                                var mainBbNumberData = mainTableData[item];

                                var data = BBData.find(p => p.bbNumber == mainBbNumberData.bbNumber)
                                var selectedSeatsByBB = selectedSeats.filter(p => p.bbNumber === data.bbNumber);
                                return (
                                    <CListGroup accent>
                                        <CListGroupItem accent="primary" className="d-flex justify-content-between align-items-center">
                                            {data.bbNumber}
                                            <h5>
                                                <CBadge color="info">
                                                    {data.seats.length} / {selectedSeatsByBB.length}
                                                </CBadge>
                                            </h5>

                                            <CButton style={{ width: "50px", textAlign: "center" }} onClick={() => onRemoveSelectedHeaderClick(item, data)} block color="danger"><CancelOutlined /></CButton>
                                        </CListGroupItem>
                                    </CListGroup>
                                )
                            })
                        }
                    </CCol>
                </CModalBody>
                <CModalFooter>
                    <CButton color="success" onClick={() => printProcess()}>Onalya</CButton>{' '}
                    <CButton color="secondary" onClick={() => setConfirmationModalVisibility(!confirmationModalVisibility)}>İptal</CButton>
                </CModalFooter>
            </CModal>
        </Layout>
    )
}

export default MainPage;